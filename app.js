const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) => {
  res.send('This is my Version 2 App')
})

app.listen(port, () => {
  console.log('Example app listening at port %s', port)
})

